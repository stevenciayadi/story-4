from django import forms

class ScheduleForm(forms.Form):

	Day = forms.CharField(widget=forms.TextInput(attrs={
		'class': 'form-control',
		'placeholder': 'Day',
		'type' : 'text',
		'required' : True,
	}))

	Date = forms.DateField(widget=forms.TextInput(attrs={
		'class': 'form-control',
		'placeholder': 'dd/mm/yyyy',
		'type' : 'date',
		'required' : True,
	}))

	Time = forms.TimeField(widget=forms.TextInput(attrs={
		'class': 'form-control',
		'type' : 'time',
		'required' : True,
	}))

	Name = forms.CharField(widget=forms.TextInput(attrs={
		'class': 'form-control',
		'placeholder': 'Activity Name',
		'type' : 'text',
		'required' : True,
	}))
from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name = 'index'),
    path('Schedule/', views.Schedule, name = 'Schedule'),
    path('<int:pk>',views.Delete, name = 'Delete')
]
from django.shortcuts import render, redirect
from .models import index as Jadwal
from .forms import ScheduleForm

# Create your views here.
def index(request):
    return render(request, 'index.html')

def Schedule(request):
	if request.method == "POST":
		form = ScheduleForm(request.POST)
		if form.is_valid():
			sched = Jadwal()
			sched.Day = form.cleaned_data['Day']
			sched.Date = form.cleaned_data['Date']
			sched.Time = form.cleaned_data['Time']
			sched.Name= form.cleaned_data['Name']
			sched.save()
		return redirect('/Schedule')
	else:
		schedule = Jadwal.objects.all()
		form = ScheduleForm()
		response = {"sched":schedule, 'form' : form}
		return render(request,'Schedule.html', response)

def Delete(request, pk):
    if request.method == "POST":
        form = ScheduleForm(request.POST)
        if form.is_valid():
            sched = Jadwal()
            sched.Day = form.cleaned_data['Day']
            sched.Date = form.cleaned_data['Date']
            sched.Time = form.cleaned_data['Time']
            sched.Name = form.cleaned_data['Name']
            sched.save()
        return redirect('/Schedule')
    else:
        Jadwal.objects.filter(pk=pk).delete()
        data = Jadwal.objects.all()
        form = ScheduleForm()
        response = {"sched":data, 'form' : form}
        return render(request, 'Schedule.html', response)

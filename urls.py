from django.urls import path, include
from django.contrib import admin

urlpatterns = [
    re_path('admin/', admin.site.urls),
    re_path('', include('homepage.urls')),
]